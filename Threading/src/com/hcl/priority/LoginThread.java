package com.hcl.priority;

public class LoginThread implements Runnable {
	private String name;
	private String password;
	private long accNo;
	private double balance;
	
	
	public LoginThread() {
		super();
	}
	public LoginThread(String name, String password, long accNo, double balance) {
		super();
		this.name = name;
		this.password = password;
		this.accNo = accNo;
		this.balance = balance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public long getAccNo() {
		return accNo;
	}




	public void setAccNo(long accNo) {
		this.accNo = accNo;
	}




	public double getBalance() {
		return balance;
	}




	public void setBalance(double balance) {
		this.balance = balance;
	}




	




	@Override
	public void run() {
		if(name.length()==10 && password.length()>=10) {
			System.out.println("Verified user");
		}
		else {
			System.err.println("Invalid user");
			System.exit(0);
		}
		

	}

}
