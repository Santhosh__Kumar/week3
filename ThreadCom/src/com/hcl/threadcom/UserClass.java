package com.hcl.threadcom;

import java.util.Scanner;

public class UserClass extends Thread{

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the amount to be withdrawn");
		double amount=sc.nextDouble();
		AtmClass atm=new AtmClass();
		atm.setWithdrawReq(amount);
		if(!(atm.withdrawMoney())) {
			System.out.println("Oops atm has low amount than you wanted\nWait for a sec while our admin updates the balance");
Admin admin=new Admin();
			
			synchronized (admin) {
			try {
				admin.start();
				admin.wait();
				System.out.println("Atm balance updated");
				System.out.println("you can withdraw upto "+atm.getAtmBalance());
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
			
		}
		else {
			
			System.out.println("Withdrawl successful");
			System.out.println(atm.getAtmBalance());
			
		}
		
		

	}

}
