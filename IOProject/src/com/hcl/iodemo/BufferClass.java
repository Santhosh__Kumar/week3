package com.hcl.iodemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BufferClass {

	public static void main(String[] args) {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		try {
			System.out.println("Enter your name");
			String str=br.readLine();
			int n=0;
			
//			while((n=br.read())!=-1) {
//				System.out.println((char)n);
//				
//				
//			}
			System.out.println("Enter your age:");
			int age=Integer.parseInt(br.readLine());
			System.out.println("Of age "+age);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
