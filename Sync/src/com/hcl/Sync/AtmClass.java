package com.hcl.Sync;

public class AtmClass {
	protected static double atmBalance=0;

	public AtmClass() {
		super();
	}

	public AtmClass(Double atmBalance) {
		super();
		this.atmBalance = atmBalance;
	}

	public double getAtmBalance() {
		return atmBalance;
	}

	public void setAtmBalance(Double atmBalance) {
		this.atmBalance = atmBalance;
	}
	

	
}
