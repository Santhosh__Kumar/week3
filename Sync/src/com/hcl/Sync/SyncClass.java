package com.hcl.Sync;
import java.util.Scanner;
public class SyncClass implements Runnable {
	Scanner sc=new Scanner(System.in);
	@Override
	public  void run() {
		System.out.println(Thread.currentThread().getName()+" is using the machine currently");
		AtmClass atm=new AtmClass();
		System.out.println("You can withdraw any amount within "+atm.getAtmBalance());
		double withdrawlAmount=sc.nextDouble();
	}

	public static void main(String[] args) {
		Thread adminThread=new Thread(new AdminClass(),"Admin");
		Thread userThread =new Thread(new SyncClass(),"User");
		adminThread.setPriority(Thread.MAX_PRIORITY);
		userThread.setPriority(Thread.MIN_PRIORITY);
		try {
			userThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		adminThread.start();
		userThread.start();
		

	}

	

}
